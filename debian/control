Source: g2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libtool,
               libx11-dev,
               chrpath,
               libgd-dev,
               perl-xs-dev,
               xutils-dev
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/med-team/g2
Vcs-Git: https://salsa.debian.org/med-team/g2.git
Homepage: https://github.com/danielrmeyer/g2/
Rules-Requires-Root: no

Package: libg2-dev
Architecture: any
Section: libdevel
Depends: libg20 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: g2 2D graphics library (development files)
 g2 is an easy to use, portable and powerful 2D graphics library. It provides a
 comprehensive set of functions for simultaneous generation of graphical output
 to X11 and graphic formats PNG, JPEG and PostScript. g2 is written in ANSI C
 and provides Fortran and Perl interfaces.
 .
 This package provides static library and header files for g2.

Package: libg20
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: g2 2D graphics library
 g2 is an easy to use, portable and powerful 2D graphics library. It provides a
 comprehensive set of functions for simultaneous generation of graphical output
 to X11 and graphic formats PNG, JPEG and PostScript. g2 is written in ANSI C
 and provides Fortran and Perl interfaces.

Package: libg20-perl
Architecture: any
Section: perl
Depends: ${shlibs:Depends},
         libg20 (>= ${binary:Version}),
         ${perl:Depends},
         ${misc:Depends}
Description: g2 2D graphics library (Perl module)
 g2 is an easy to use, portable and powerful 2D graphics library. It provides a
 comprehensive set of functions for simultaneous generation of graphical output
 to X11 and graphic formats PNG, JPEG and PostScript. g2 is written in ANSI C
 and provides Fortran and Perl interfaces.
 .
 This package provides the g2 perl module.
